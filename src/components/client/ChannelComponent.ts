import {Application, Component, Configurable, Inject} from '@lakutata/core'
import {ClientComponent} from './ClientComponent'
import {Channel} from '../../lib/Channel'
import {Logger} from '@lakutata/core/build/plugins/Logger'
import {TChannelEvent} from '../../types/TChannelEvent'
import HyperID from 'hyperid'
import {Service} from '../../Service'
import {
    SERVICE_CHANNEL_PUBLISH_EVENT,
    SERVICE_CHANNEL_SUBSCRIBE_EVENT
} from '../../constants/SocketConstant'
import {ON_ANY_EVENT_RESERVED_SYMBOL} from '../../constants/ModuleConstant'

export class ChannelComponent extends Component {

    @Inject(Application)
    public readonly app: Application

    @Inject('client')
    protected client: ClientComponent

    @Configurable()
    public readonly service: Service

    @Configurable()
    protected readonly logger: () => Logger | undefined

    protected readonly hyperIdInstance: HyperID.Instance = HyperID({fixedLength: true, urlSafe: true})

    protected readonly channelMap: Map<string, Channel> = new Map()

    public get channelEvents(): { [key: string]: string[] } {
        const channelEvents: { [key: string]: string[] } = {}
        this.channelMap.forEach((channel: Channel, name: string) => {
            if (!channelEvents[name]) {
                channelEvents[name] = []
            }
            channel.eventEmitter.eventNames().forEach(eventName => {
                if (typeof eventName === 'string') channelEvents[name].push(eventName)
            })
        })
        return channelEvents
    }

    protected async initialize(): Promise<void> {
        this.client.onMessage(SERVICE_CHANNEL_SUBSCRIBE_EVENT, (eventStringPacket: string) => {
            const eventPacket: TChannelEvent = this.app.JSON.parse(eventStringPacket)
            this.channelMap.get(eventPacket.channel)?.eventEmitter.emit(ON_ANY_EVENT_RESERVED_SYMBOL, eventPacket.event, ...eventPacket.args)
            const isSuccess: boolean = !!(this.channelMap.get(eventPacket.channel)?.eventEmitter.emit(eventPacket.event, ...eventPacket.args))
            if (isSuccess) {
                this.logger()?.info(
                    eventPacket.id,
                    `[RECEIVED-SUCCESS]`,
                    'from',
                    `[${eventPacket.source}]`,
                    eventPacket.channel,
                    eventPacket.event,
                    `(${eventPacket.mode})`
                )
            } else {
                this.logger()?.warning(
                    eventPacket.id,
                    `[RECEIVED-FAILED]`,
                    'from',
                    `[${eventPacket.source}]`,
                    eventPacket.channel,
                    eventPacket.event,
                    `(${eventPacket.mode})`
                )
            }
        })
    }

    /**
     * 创建新通讯通道
     * @param name
     * @protected
     */
    protected createChannel(name: string): Channel {
        return new Channel(name, this)
    }

    /**
     * 构建通道事件数据包
     * @param channel
     * @param appId
     * @param eventName
     * @param mode
     * @param args
     * @protected
     */
    protected buildEventPacket(channel: string, appId: string | undefined, eventName: string, mode: 'all' | 'one', ...args: any[]): string {
        const packet: TChannelEvent = {
            id: this.hyperIdInstance(),
            channel: channel,
            event: eventName,
            mode: mode,
            source: this.app.getID(),
            app: appId,
            args: args ? args : []
        }
        return this.app.JSON.stringify(packet)
    }

    /**
     * 发布渠道事件
     * @param channel
     * @param appId
     * @param eventName
     * @param mode
     * @param args
     */
    public publishEvent(channel: string, appId: string | undefined, eventName: string, mode: 'all' | 'one', ...args: any[]): boolean {
        return this.client.send(SERVICE_CHANNEL_PUBLISH_EVENT, this.buildEventPacket(channel, appId, eventName, mode, ...args))
    }

    /**
     * 获取通讯通道
     * @param name
     */
    public get(name: string): Channel {
        if (!this.channelMap.has(name)) {
            this.channelMap.set(name, this.createChannel(name))
        }
        return this.channelMap.get(name)!
    }

    /**
     * 获取通讯通道列表
     */
    public list(): string[] {
        const channelNames: string[] = []
        this.channelMap.forEach((channel: Channel, name: string) => channelNames.push(name))
        return channelNames
    }
}
