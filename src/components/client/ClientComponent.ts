import {Application, Component, Configurable, Inject} from '@lakutata/core'
import {Logger} from '@lakutata/core/build/plugins/Logger'
import {connect, Socket} from 'socket.io-client'
import {
    CONNECT_PATH, DEFAULT_PORT, FETCH_SERVICES_EVENT, SERVICE_APP_ID,
    SERVICE_APP_NAME, SERVICE_PING_EVENT,
    TIMESTAMP_PARAM,
    TRANSPORTS
} from '../../constants/SocketConstant'
import {DisconnectDescription} from 'socket.io-client/build/esm/socket'
import {RequestTimeoutException} from '../../exceptions/RequestTimeoutException'
import {RequestException} from '../../exceptions/RequestException'
import parseUrl from 'parse-url'
import {InvalidRegistryProtocolException} from '../../exceptions/InvalidRegistryProtocolException'
import {ServiceClientInitializationException} from '../../exceptions/ServiceClientInitializationException'
import {Service} from '../../Service'
import {ProviderComponent} from './ProviderComponent'
import {PING_EVENT} from '../../constants/ModuleConstant'
import {ChannelComponent} from './ChannelComponent'
import {IServiceInfoObject} from '../../interfaces/IServiceInfoObject'
import {ConnectionNotReadyException} from '../../exceptions/ConnectionNotReadyException'
import {format} from 'util'

export class ClientComponent extends Component {

    @Inject(Application)
    protected readonly app: Application

    @Configurable()
    protected readonly service: Service

    @Configurable()
    protected readonly registry: () => string

    @Configurable()
    protected readonly token: () => string

    @Configurable()
    protected readonly logger: () => Logger | undefined

    protected readonly acceptProtocols: string[] = ['http', 'https']

    protected readonly registryPattern: RegExp = /^(([^:/?#]+):)?\/\/(([^/?#]+):(.+)@)?([^/?#:]*)(:(\d+))?([^?#]*)(\\?([^#]*))?(#(.*))?/

    protected readonly pingInterval: number = 3000

    protected socket: Socket

    protected latency: number = 0

    /**
     * 初始化函数
     * @protected
     */
    protected async initialize(): Promise<void> {
        return new Promise((resolve, reject) => {
            try {
                let portInRegistry: string = ''
                let registryPatternInput: string = this.registry()
                if (!this.registryPattern.test(registryPatternInput)) {
                    registryPatternInput = `registry://${registryPatternInput}`
                }
                if (this.registryPattern.test(registryPatternInput)) {
                    const matches = registryPatternInput.match(this.registryPattern)
                    if (matches) {
                        portInRegistry = matches[8] || ''
                    }
                }
                if (!this.registry()) return reject(this.generateException(ServiceClientInitializationException, 'Service registry address not set'))
                const parsedUrl = parseUrl(this.registry(), true)
                if (!this.acceptProtocols.includes(parsedUrl.protocol)) return reject(this.generateException(InvalidRegistryProtocolException, `Invalid registry protocol [${parsedUrl.protocol}]`))
                parsedUrl.port = parsedUrl.port ? parsedUrl.port : portInRegistry ? portInRegistry : DEFAULT_PORT.toString()
                const urlObject = new URL(parsedUrl.href)
                urlObject.port = parsedUrl.port
                this.socket = connect(urlObject.toString(), {
                    autoConnect: false,//使用当前类内connect方法进行连接
                    reconnection: true,//需要设定为自动重连，否则已绑定socket的各种事件将不容易处理
                    transports: TRANSPORTS,
                    path: CONNECT_PATH,
                    timestampParam: TIMESTAMP_PARAM,
                    timestampRequests: true,
                    extraHeaders: {
                        [SERVICE_APP_ID]: Buffer.from(this.app.getID()).toString('base64'),
                        [SERVICE_APP_NAME]: Buffer.from(this.app.getName()).toString('base64')
                    },
                    auth: (cb: (data: object) => void) => {
                        cb({
                            token: this.token(),
                            patterns: this.service.Components.get<ProviderComponent>('provider').getPatterns(),
                            events: this.service.Components.get<ChannelComponent>('channel').channelEvents
                        })
                    }
                })
                //定时向注册中心汇报自身状况
                setInterval(() => this.ping(), this.pingInterval)
                this.service.on(PING_EVENT, () => this.ping())
                return resolve()
            } catch (e) {
                return reject(this.generateException(ServiceClientInitializationException, (e as Error).message))
            }
        })
    }

    /**
     * 向注册中心发送ping包
     * @protected
     */
    protected ping(): void {
        try {
            const latencyTestStart: number = Date.now()
            let tasks: number = this.service.Components.get<ProviderComponent>('provider')?.getTasks()
            tasks = tasks ? tasks : 0
            this.socket.volatile.emit(SERVICE_PING_EVENT, {
                events: this.service.Components.get<ChannelComponent>('channel').channelEvents,
                latency: this.latency,
                tasks: tasks
            }, () => {
                this.latency = Date.now() - latencyTestStart
            })
        } catch (e) {
            this.app.Logger.error('Service client ping error:', (e as Error).message)
        }
    }

    /**
     * 连接服务注册中心
     */
    public async connect(): Promise<void> {
        return new Promise((resolve, reject) => {
            try {
                let fulfilled: boolean = false
                this.socket
                    .on('connect', async () => {
                        if (!fulfilled) {
                            fulfilled = true
                            return resolve()
                        } else {
                            this.service.emit(PING_EVENT)
                            return this.logger()?.info('Service client reconnected to registry', this.registry())
                        }
                    })
                    .on('connect_error', (connectError: Error) => {
                        return this.logger()?.error('Service client cannot connect to', this.registry(), ':', connectError.message)
                    })
                    .on('disconnect', (reason: Socket.DisconnectReason, description: DisconnectDescription | undefined) => {
                        const desc: string = (description && !(description instanceof Error)) ? description.description : ''
                        return this.logger()?.warning('Service client disconnected from', this.registry(), ':', reason, desc ? `(${desc})` : '')
                    })
                    .connect()
            } catch (e) {
                return reject(e)
            }
        })
    }

    /**
     * 发送不需要响应的请求数据
     * @param event
     * @param data
     */
    public send(event: string, data: any): boolean {
        try {
            if (this.socket.connected) {
                this.socket.emit(event, data)
                return true
            }
            return false
        } catch (e) {
            this.app.Logger.error('Send data error:', (e as Error).message)
            return false
        }
    }

    /**
     * 发送请求
     * @param event
     * @param data
     * @param timeout
     */
    public async request(event: string, data: any, timeout: number = -1): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                if (this.socket.connected) {
                    if (timeout > -1) {
                        this.socket.timeout(timeout).emit(event, data, (error: Error, response: any) => error ? reject(this.generateException(RequestTimeoutException, `Local request timeout after ${timeout}ms, payload: ${format(data)}`)) : resolve(response))
                    } else {
                        this.socket.emit(event, data, (response: any) => resolve(response))
                    }
                } else {
                    reject(this.generateException(ConnectionNotReadyException, 'Local connection not ready'))
                }
            } catch (e) {
                reject(this.generateException(RequestException, (e as Error).message))
            }
        })
    }

    /**
     * 响应请求
     * @param event
     * @param callback
     */
    public onRequest(event: string, callback: (data: any) => Promise<any> | any): this {
        this.socket.on(event, async (data: any, fn: (response: any) => void) => {
            if (typeof fn === 'function') fn(await callback(data))
        })
        return this
    }

    /**
     * 处理不需要响应的请求
     * @param event
     * @param callback
     */
    public onMessage(event: string, callback: (data: any) => Promise<void> | void): this {
        this.socket.on(event, (data: any) => {
            return callback(data)
        })
        return this
    }

    /**
     * 从Registry获取集群内的所有服务信息
     * @param appId
     */
    public async fetchServices(appId?: string | RegExp): Promise<IServiceInfoObject> {
        const services: IServiceInfoObject = {}
        const rawJsonStr: string = await this.request(FETCH_SERVICES_EVENT, null)
        const parsedResponse: { [key: string]: any[] } = JSON.parse(rawJsonStr)
        const appIds: string[] = Object.keys(parsedResponse)
        appIds.forEach(appId => {
            services[appId] = parsedResponse[appId].map(value => {
                return Object.assign({}, value, {
                    events: new Map(Object.entries(value.events))
                })
            })
        })
        if (appId) {
            const regExp: RegExp = typeof appId === 'string' ? new RegExp(appId) : appId
            Object.keys(services).forEach((serviceAppId: string) => {
                if (!regExp.test(serviceAppId)) {
                    delete services[serviceAppId]
                }
            })
        }
        return services
    }
}
