export const IS_CONTROLLER: string = 'lakutata_service::is_controller'
export const CONTROLLER_BASIC_PATTERN: string = 'lakutata_service::basic_pattern'
export const CONTROLLER_PATTERN_MAP: string = 'lakutata_service::pattern_map'
