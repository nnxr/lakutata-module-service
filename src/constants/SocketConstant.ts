export const DEFAULT_PORT: number = 6911
export const TRANSPORTS: ('polling' | 'websocket')[] = ['websocket']
export const CONNECT_PATH: string = '/'
export const MAX_BUFFER_SIZE: number = 104857600
export const SERVICE_APP_ID: string = 'sai'//由于http请求中header的key字段必须为小写，则该处的常量必须为小写
export const SERVICE_APP_NAME: string = 'san'//由于http请求中header的key字段必须为小写，则该处的常量必须为小写
export const TIMESTAMP_PARAM: string = 'tsp'

export const SERVICE_ONLINE_EVENT: string = '$SONE'
export const SERVICE_OFFLINE_EVENT: string = '$SOFFE'
export const SERVICE_UPDATE_EVENT: string = '$SUE'
export const SERVICE_PING_EVENT: string = '$SPE'
export const FETCH_SERVICES_EVENT: string = '$FSE'
export const SERVICE_INVOKE_EVENT: string = '$SIE'
export const SERVICE_PROXY_INVOKE_EVENT: string = '$SPIE'

export const SERVICE_CHANNEL_PUBLISH_EVENT: string = '$SCPE'
export const SERVICE_CHANNEL_SUBSCRIBE_EVENT: string = '$SCSE'

export const SERVICE_INVOKE_ID: string = '$SERVICE_ID'

