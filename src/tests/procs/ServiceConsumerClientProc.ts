import {createApp, Process} from '@lakutata/core'
import {Service} from '../../Service'
import {registryPort, registryToken} from '../conts/CommonConstant'

export class ServiceConsumerClientProc extends Process {
    protected initialize(): Promise<void> | void {
        createApp({
            id: 'test.test.app',
            name: 'test.test.app',
            modules: {
                service: {
                    class: Service,
                    registry: `127.0.0.1:${registryPort}`,
                    token: registryToken,
                    logger: true
                }
            }
        }).then(app => {
            const consumer: Service = app.Modules.get<Service>('service')
            consumer.channel('test').onAny(console.log)
            consumer.channel('test').on('test', console.log)
            // consumer.fetchServices().then(console.log)
            consumer.invoke({
                serviceId: app.getID(),
                data: {
                    test: 1,
                    opr: 'add',
                    a: 1,
                    b: 2
                },
                transform: response => {
                    response.a = response.sum
                    return response
                }
            }, {
                serviceId: app.getID()
            }).then(console.log).catch(console.error)
        }).catch(e => {
            console.error(e)
        })
    }
}
