import {createApp, Process} from '@lakutata/core'
import {Service} from '../../Service'
import path from 'path'
import {registryPort, registryToken} from '../conts/CommonConstant'

export class ServiceProviderClientProc extends Process {
    protected initialize(): Promise<void> | void {
        createApp({
            id: 'test.test.app',
            name: 'test.test.app',
            modules: {
                service: {
                    class: Service,
                    registry: `127.0.0.1:${registryPort}`,
                    token: registryToken,
                    controllers: [
                        `${path.resolve(__dirname, '../ctrls')}/**/*`
                    ],
                    logger: true
                }
            }
        }).then(app => {
            const provider: Service = app.Modules.get<Service>('service')
            setTimeout(() => {
                provider.channel('test').triggerAll('test', 'hello!', 'all')
                provider.channel('test').triggerOne('test', 'hello!', 'one')
                // provider.fetchServices().then(console.log).catch(console.error)
            }, 1000)
        }).catch(e => {
            console.error(e)
        })
    }
}
