import {createApp, Process} from '@lakutata/core'
import {ServiceRegistry} from '../../ServiceRegistry'
import {registryPort, registryToken} from '../conts/CommonConstant'

export class ServiceRegistry2Proc extends Process {
    protected initialize(): Promise<void> | void {
        createApp({
            id: 'registry.test.app',
            name: 'registry2.test.app',
            modules: {
                registry: {
                    class: ServiceRegistry,
                    port: registryPort + 1,
                    token: registryToken,
                    redisOptions: {
                        port: 6379,
                        host: '192.168.0.147',
                        password: '20160329',
                        db: 8
                    },
                    logger: true
                }
            }
        }).then(app => {
            const serviceRegistry: ServiceRegistry = app.Modules.get<ServiceRegistry>('registry')
        }).catch(e => {
            console.error(e)
        })
    }
}
