import {createApp} from '@lakutata/core'
import {ServiceProviderClientProc} from './procs/ServiceProviderClientProc'
import {ServiceConsumerClientProc} from './procs/ServiceConsumerClientProc'
import {ServiceRegistry} from '../ServiceRegistry'
import {registryPort, registryToken} from './conts/CommonConstant'

createApp({
    id: 'test.app',
    name: 'tester',
    processes: [ServiceProviderClientProc, ServiceConsumerClientProc],
    modules: {
        registry: {
            class: ServiceRegistry,
            port: registryPort,
            token: registryToken,
            redisOptions: {
                port: 6379,
                host: '192.168.0.147',
                password: '20160329',
                db: 8
            },
            logger: false
        }
    }
}).catch(console.error)

