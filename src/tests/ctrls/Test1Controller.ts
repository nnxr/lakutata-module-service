import {BaseController, Controller, PATTERN, TPattern} from '../../ServiceModule'
import {Exception} from '@lakutata/core'

let bigArr: number[] = []
for (let i = 0; i < (65535 * 50); i++) bigArr.push(255)

class TestException extends Exception {
    district: string
    errno: number | string = 'E_TEST'
}

@Controller({test: 1})
export class Test1Controller extends BaseController {

    @PATTERN({opr: 'add'})
    public async add(inp: TPattern) {
        throw this.generateException(TestException, 'this is test exception')
        return {
            sum: inp.a + inp.b,
            // big: bigArr
        }
    }

}
