import {Application, Configurable, IComponentsConfig, Inject, Module, Validator} from '@lakutata/core'
import {TInvokeObject} from './types/TInvokeObject'
import {ClientComponent} from './components/client/ClientComponent'
import {ProviderComponent} from './components/client/ProviderComponent'
import {ConsumerComponent} from './components/client/ConsumerComponent'
import {ChannelComponent} from './components/client/ChannelComponent'
import {Channel} from './lib/Channel'
import {IServiceInfoObject} from './interfaces/IServiceInfoObject'

export class Service extends Module {

    @Inject(Application)
    protected readonly app: Application

    @Configurable()
    protected readonly registry: string

    @Configurable()
    protected readonly token: string = ''

    @Configurable()
    protected readonly logger: boolean = false

    @Configurable()
    protected readonly controllers: string | string[] = []

    @Configurable()
    protected readonly timeout: number = 60000

    @Configurable()
    protected readonly retry: number = 0

    protected readonly components: IComponentsConfig = {
        client: {
            class: ClientComponent,
            service: this,
            registry: () => this.registry,
            token: () => this.token,
            logger: () => this.logger ? this.app.Logger : undefined
        },
        provider: {
            class: ProviderComponent,
            service: this,
            controllers: () => this.controllers,
            logger: () => this.logger ? this.app.Logger : undefined
        },
        consumer: {
            class: ConsumerComponent,
            timeout: () => this.timeout,
            retry: () => this.retry,
            logger: () => this.logger ? this.app.Logger : undefined
        },
        channel: {
            class: ChannelComponent,
            service: this,
            logger: () => this.logger ? this.app.Logger : undefined
        }
    }

    /**
     * 初始化函数
     * @protected
     */
    protected async initialize(): Promise<void> {
        await this.Components.get<ClientComponent>('client').connect()
        this.app.Logger.info('Service client connected to', this.registry)
    }

    /**
     * 获取服务应用程序列表
     * @param appId
     */
    public async fetchServices(appId?: string | RegExp): Promise<IServiceInfoObject> {
        return await this.Components.get<ClientComponent>('client').fetchServices(appId)
    }

    /**
     * 单个或链式调用
     * @param params
     */
    public async invoke(...params: TInvokeObject[]): Promise<any> {
        return await this.Components.get<ConsumerComponent>('consumer').invoke(null, ...params)
    }

    /**
     * 并行调用
     * @param params
     */
    public async parallel(...params: (TInvokeObject | TInvokeObject[])[]): Promise<any[]> {
        return await this.Components.get<ConsumerComponent>('consumer').parallelInvoke(...params)
    }

    /**
     * 对指定通道进行操作
     * @param name
     */
    public channel(name: string): Channel {
        return this.Components.get<ChannelComponent>('channel').get(name)
    }

    /**
     * 获取所有通道名称数组
     */
    public channels(): string[] {
        return this.Components.get<ChannelComponent>('channel').list()
    }

}
