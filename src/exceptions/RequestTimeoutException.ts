import {Exception} from '@lakutata/core'

export class RequestTimeoutException extends Exception {
    district: string
    errno: number | string = 'E_REQUEST_TIMEOUT'
}
