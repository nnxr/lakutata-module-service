import {Exception} from '@lakutata/core'

export class ServiceClientInitializationException extends Exception {
    district: string
    errno: number | string = 'E_SERVICE_CLIENT_INITIALIZATION'
}
