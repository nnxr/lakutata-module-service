import {Exception} from '@lakutata/core'

export class PatternNotFoundException extends Exception {
    district: string
    errno: number | string = 'E_PATTERN_NOT_FOUND'

    protected templates(): string[] {
        return [
            'No matched pattern for {0}'
        ]
    }
}
