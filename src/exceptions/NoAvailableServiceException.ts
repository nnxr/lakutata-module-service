import {Exception} from '@lakutata/core'

export class NoAvailableServiceException extends Exception {
    district: string
    errno: number | string = 'E_NO_AVAILABLE_SERVICE'
}
