import {Exception} from '@lakutata/core'

export class ServiceRegistryInitializationException extends Exception {
    district: string
    errno: number | string = 'E_SERVICE_REGISTRY_INITIALIZATION'
}
