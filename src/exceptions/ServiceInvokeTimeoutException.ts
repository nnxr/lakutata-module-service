import {Exception} from '@lakutata/core'

export class ServiceInvokeTimeoutException extends Exception {
    district: string
    errno: number | string = 'E_SERVICE_INVOKE_TIMEOUT'

    protected templates(): string[] {
        return [
            'Service invoke {0} timeout after {1} milliseconds'
        ]
    }
}
