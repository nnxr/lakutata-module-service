import {Exception} from '@lakutata/core'

export class RequestException extends Exception {
    district: string
    errno: number | string = 'E_REQUEST'
}
