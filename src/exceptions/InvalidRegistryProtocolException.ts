import {Exception} from '@lakutata/core'

export class InvalidRegistryProtocolException extends Exception {
    district: string
    errno: number | string = 'E_INVALID_REGISTRY_PROTOCOL'
}
