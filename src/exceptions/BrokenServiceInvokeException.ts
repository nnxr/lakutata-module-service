import {Exception} from '@lakutata/core'

export class BrokenServiceInvokeException extends Exception {
    district: string
    errno: number | string = 'E_BROKEN_SERVICE_INVOKE'
}
