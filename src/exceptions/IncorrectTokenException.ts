import {Exception} from '@lakutata/core'

export class IncorrectTokenException extends Exception {
    district: string
    errno: number | string = 'E_INCORRECT_TOKEN'
}
