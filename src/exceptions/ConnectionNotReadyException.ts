import {Exception} from '@lakutata/core'

export class ConnectionNotReadyException extends Exception {
    district: string
    errno: number | string = 'E_CONNECTION_NOT_READY'
}
