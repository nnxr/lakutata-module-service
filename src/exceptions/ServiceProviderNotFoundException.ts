import {Exception} from '@lakutata/core'

export class ServiceProviderNotFoundException extends Exception {
    district: string
    errno: number | string = 'E_SERVICE_PROVIDER_NOT_FOUND'

    protected templates(): string[] {
        return [
            'Service provider {0} not found'
        ]
    }
}
