import {
    Application,
    Configurable,
    IComponentsConfig, Inject,
    Module
} from '@lakutata/core'
import {RegistryComponent} from './components/registry/RegistryComponent'
import {RedisOptions} from 'ioredis'
import {RegistryAdapterComponent} from './components/registry/RegistryAdapterComponent'
import {DEFAULT_PORT} from './constants/SocketConstant'
import {IServiceInfoObject} from './interfaces/IServiceInfoObject'

export class ServiceRegistry extends Module {

    @Inject(Application)
    protected readonly app: Application

    @Configurable()
    protected readonly port: number = DEFAULT_PORT

    @Configurable()
    protected readonly token: string = ''

    @Configurable()
    protected readonly logger: boolean = false

    @Configurable()
    protected readonly redisOptions: RedisOptions | undefined = undefined

    protected readonly components: IComponentsConfig = {
        registry: {
            class: RegistryComponent,
            cleanDeathSocketsInterval: 3000,
            port: () => this.port,
            token: () => this.token,
            logger: () => this.logger
        },
        registryAdapter: {
            class: RegistryAdapterComponent,
            redisOptions: () => this.redisOptions
        }
    }

    /**
     * 模块初始化函数
     * @protected
     */
    protected async initialize(): Promise<void> {
        this.app.Logger.info('Service registry listening on port:', this.port)
    }

    /**
     * 获取服务应用程序列表
     * @param appId
     */
    public fetchServices(appId?: string | RegExp): IServiceInfoObject {
        return this.Components.get<RegistryComponent>('registry').fetchServices(appId)
    }
}
