import {TPattern} from '../types/TPattern'
import {IConstructor} from '@lakutata/core'
import {BaseController} from '../lib/BaseController'
import {TMethod} from '../types/TMethod'
import {CONTROLLER_PATTERN_MAP} from '../constants/MetadataKey'

/**
 * Pattern处理方法
 * @param target
 * @param propertyKey
 * @param descriptor
 * @param pattern
 */
function patternHandler(target: Object, propertyKey: string | symbol, descriptor: TypedPropertyDescriptor<TMethod>, pattern: TPattern): void {
    const controllerConstructor: IConstructor<BaseController> = target.constructor as IConstructor<BaseController>
    if (!Reflect.getMetadata(CONTROLLER_PATTERN_MAP, controllerConstructor)) Reflect.defineMetadata(CONTROLLER_PATTERN_MAP, new Map(), controllerConstructor)
    Reflect.getMetadata(CONTROLLER_PATTERN_MAP, controllerConstructor).set(propertyKey, pattern)
}

/**
 * 服务方法pattern修饰器
 * @param pattern
 * @constructor
 */
export function PATTERN(pattern: TPattern) {
    return (target: Object, propertyKey: string | symbol, descriptor: TypedPropertyDescriptor<TMethod>): void => {
        patternHandler(target, propertyKey, descriptor, pattern)
    }
}
