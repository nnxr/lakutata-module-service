import {IConstructor} from '@lakutata/core'
import {CONTROLLER_BASIC_PATTERN, CONTROLLER_PATTERN_MAP, IS_CONTROLLER} from '../constants/MetadataKey'
import {TPattern} from '../types/TPattern'
import {BaseController} from '../lib/BaseController'

/**
 * 服务控制器类修饰器
 * @constructor
 */
export function Controller(basicPattern: TPattern = {}) {
    return <TController extends BaseController = BaseController>(target: IConstructor<TController>) => {
        if (!Reflect.getMetadata(IS_CONTROLLER, target)) Reflect.defineMetadata(IS_CONTROLLER, true, target)
        if (!Reflect.getMetadata(CONTROLLER_BASIC_PATTERN, target)) Reflect.defineMetadata(CONTROLLER_BASIC_PATTERN, basicPattern, target)
        const patternMap: Map<string, TPattern> = Reflect.getMetadata(CONTROLLER_PATTERN_MAP, target)
        if (patternMap) {
            patternMap.forEach((pattern: TPattern, funcName: string) => {
                const combinedPattern: TPattern = Object.assign(pattern, basicPattern)
                Reflect.getMetadata(CONTROLLER_PATTERN_MAP, target).set(funcName, combinedPattern)
            })
        } else {
            Reflect.defineMetadata(CONTROLLER_PATTERN_MAP, new Map(), target)
        }
    }
}
