import {Application, BaseObject, Configurable, Inject} from '@lakutata/core'
import {Logger} from '@lakutata/core/build/plugins/Logger'

export class BaseController extends BaseObject {
    @Inject(Application)
    protected app: Application

    @Configurable()
    protected logger?: Logger | undefined
}
