import EventEmitter from 'events'
import {ChannelComponent} from '../components/client/ChannelComponent'
import {ON_ANY_EVENT_RESERVED_SYMBOL, PING_EVENT} from '../constants/ModuleConstant'

export class Channel {

    protected readonly name: string

    protected readonly channelComponent: ChannelComponent

    public readonly eventEmitter: EventEmitter = new EventEmitter({captureRejections: true})

    constructor(name: string, channelComponent: ChannelComponent) {
        this.name = name
        this.channelComponent = channelComponent
    }

    /**
     * 等同于triggerAll
     * @param event
     * @param args
     */
    public trigger(event: string, ...args: any[]): boolean {
        return this.triggerAll(event, ...args)
    }

    /**
     * 所有事件接受者都将被触发
     * @param event
     * @param args
     */
    public triggerAll(event: string, ...args: any[]): boolean {
        return this.channelComponent.publishEvent(this.name, undefined, event, 'all', ...args)
    }

    /**
     * 集群中每个服务仅有一个事件接受者被触发
     * @param event
     * @param args
     */
    public triggerOne(event: string, ...args: any[]): boolean {
        return this.channelComponent.publishEvent(this.name, undefined, event, 'one', ...args)
    }

    /**
     * 等同于directTriggerAll
     * @param appId
     * @param event
     * @param args
     */
    public directTrigger(appId: string, event: string, ...args: any[]): boolean {
        return this.directTriggerAll(appId, event, ...args)
    }

    /**
     * 向指定的服务触发triggerAll
     * @param appId
     * @param event
     * @param args
     */
    public directTriggerAll(appId: string, event: string, ...args: any[]): boolean {
        return this.channelComponent.publishEvent(this.name, appId, event, 'all', ...args)
    }

    /**
     * 向指定的服务触发triggerOne
     * @param appId
     * @param event
     * @param args
     */
    public directTriggerOne(appId: string, event: string, ...args: any[]): boolean {
        return this.channelComponent.publishEvent(this.name, appId, event, 'one', ...args)
    }

    /**
     * 等同于privateTriggerAll
     * @param event
     * @param args
     */
    public privateTrigger(event: string, ...args: any[]): boolean {
        return this.directTrigger(this.channelComponent.app.getID(), event, ...args)
    }

    /**
     * 仅触发集群中相同服务的事件
     * @param event
     * @param args
     */
    public privateTriggerAll(event: string, ...args: any[]): boolean {
        return this.directTriggerAll(this.channelComponent.app.getID(), event, ...args)
    }

    /**
     * 仅单点触发集群中相同服务的事件
     * @param event
     * @param args
     */
    public privateTriggerOne(event: string, ...args: any[]): boolean {
        return this.directTriggerOne(this.channelComponent.app.getID(), event, ...args)
    }

    /**
     * 通讯通道内所有事件监听
     * @param listener
     */
    public onAny(listener: (event: string, ...args: any[]) => void): this {
        return this.on(ON_ANY_EVENT_RESERVED_SYMBOL, listener)
    }

    /**
     * 移除用于监听通道内所有事件指定的监听器
     * @param listener
     */
    public offAny(listener: (event: string, ...args: any[]) => void): this {
        return this.off(ON_ANY_EVENT_RESERVED_SYMBOL, listener)
    }

    /**
     * 服务事件监听
     * @param event
     * @param listener
     */
    public on(event: string, listener: (...args: any[]) => void): this {
        this.eventEmitter.on(event, (...args: any[]) => {
            return listener(...args)
        })
        this.channelComponent.service.emit(PING_EVENT)
        return this
    }

    /**
     * 服务事件监听(触发后即销毁)
     * @param event
     * @param listener
     */
    public once(event: string, listener: (...args: any[]) => void): this {
        this.eventEmitter.once(event, (...args: any[]) => {
            this.channelComponent.service.emit(PING_EVENT)
            return listener(...args)
        })
        this.channelComponent.service.emit(PING_EVENT)
        return this
    }

    /**
     * 关闭事件触发
     * @param event
     * @param listener
     */
    public off(event?: string, listener?: (...args: any[]) => void): this {
        if (event && listener) {
            this.eventEmitter.removeListener(event, listener)
        } else if (event) {
            this.eventEmitter.removeAllListeners(event)
        } else {
            this.eventEmitter.removeAllListeners()
        }
        this.channelComponent.service.emit(PING_EVENT)
        return this
    }
}
