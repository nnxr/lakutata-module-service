import {TPattern} from '../types/TPattern'

export interface IServiceClientInfo {
    socketId: string
    appId: string
    appName: string
    patterns: TPattern[]
    events: { [key: string]: string[] }
    latency: number
    tasks: number
}
