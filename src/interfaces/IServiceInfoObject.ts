import {IServiceInfo} from './IServiceInfo'

export interface IServiceInfoObject {
    [appId: string]: IServiceInfo[]
}
