export interface IServiceInfo {
    readonly socketId: string
    readonly appId: string
    readonly appName: string
    readonly events: Map<string, string[]>
    readonly latency: number
    readonly tasks: number
}
