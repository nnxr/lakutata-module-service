import {TPattern} from '../types/TPattern'

export interface IValidClientHandshakeRequest {
    readonly appId: string
    readonly appName: string
    readonly patterns: TPattern[]
    readonly events: { [key: string]: string[] }
}
