import {IPatRun} from './IPatRun'

export interface IServiceClientInstance {
    socketId: string
    appId: string
    appName: string
    pm: IPatRun
    events: Map<string, string[]>
    latency: number
    tasks: number
}
