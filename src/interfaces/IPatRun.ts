import {TPattern} from '../types/TPattern'

export interface IPatRun {
    add: (pattern: TPattern, obj: any) => void
    remove: (pattern: TPattern) => void
    find: (subject: TPattern) => any
    list: (partialPattern?: TPattern) => any
}
