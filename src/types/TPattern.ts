import {Validator} from '@lakutata/core'

export const TPatternSchema = Validator.Object(Validator.Any)

export type TPattern = { [key: string]: any }
