import {Validator} from '@lakutata/core'
import {SERVICE_APP_ID, SERVICE_APP_NAME} from '../constants/SocketConstant'

export const TClientHandshakeRequestSchema = Validator.Object({
    auth: Validator.Object({
        token: Validator.String.required(),
        patterns: Validator.Array(Validator.Object(Validator.Any)).required(),
        events: Validator.Object(Validator.Array(Validator.String)).required()
    }),
    headers: Validator.Object({
        [SERVICE_APP_ID]: Validator.String.required(),
        [SERVICE_APP_NAME]: Validator.String.required()
    })
})

export type TClientHandshakeRequest = {
    auth: any
    headers: {
        [key: string]: string | string[] | undefined
    }
}
