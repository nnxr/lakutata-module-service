export type TChannelEvent = {
    id: string
    channel: string
    event: string
    mode: 'all' | 'one'
    source: string
    app?: string
    args: any[]
}
