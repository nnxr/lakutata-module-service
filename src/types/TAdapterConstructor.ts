import {Namespace} from 'socket.io'
import {Adapter} from 'socket.io-adapter'

export type TAdapterConstructor = (typeof Adapter | ((nsp: Namespace) => Adapter));
