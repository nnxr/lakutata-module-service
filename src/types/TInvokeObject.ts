import {TPattern} from './TPattern'

export type TInvokeObject = {
    serviceId: string
} & Partial<{
    id: string
    data: TPattern
    transform: (response: any) => Promise<any> | any
    timeout: number
    timeoutFallback: (() => any | Promise<any>) | any
    retry: number
}>
