import {TPattern} from './TPattern'
import {Exception} from '@lakutata/core'

export type TInvokePattern = {
    object: TPattern | any
    error: Exception | Error | null
    retryableError?: boolean
    chain: string[]
    timeout: number
    retry: number
    [key: string]: any
}
